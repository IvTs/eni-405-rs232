#include "USART.h"

//==========================================================================================
// ������������� UART �� ������� GSM ������
//==========================================================================================
void mcuToGsmUartInit(unsigned char baud)
{
	UBRR0H = 0;
  	UBRR0L = baud;   																		// ������������� �������� ������ �� ����������������� ����������	
  	UCSR0B |= (1<<RXCIE0)|(1<<RXEN0)|(1<<TXEN0)|(1<<TXCIE0);									// ���������� ������, ��������, ��������� �� ������ � ��������
  	UCSR0C |= (1<<URSEL0)|(1<<UCSZ01)|(1<<UCSZ00);											// 8-���, 1 ����-���, ��� �������� ��������
}


//==========================================================================================
// ������������� UART �� ������� �����
//==========================================================================================
void pcToMcuUartInit(unsigned char baud)
{

	unsigned char tmp = 0x00;
	// 0 ������ ������ - �������� �� UART 			03 - 115200
	//									  			07 - 57600
	//									  			11 - 38400
	//									  			23 - 19200
	//									  			47 - 9600
	//
	// 1 ������ ������ - ������� ���� �������� 		00 - ��� �������� ��������
	// 										   		01 - ���� �������� ��������
	//
	// 2 ������ ������ - ���������� ����-�����		01 - 1 ���� ���
	//												02 - 2 ���� ����
	//
	// 3 ������ ������ - �������� 					00 - Odd
	//												01 - Even
	//  											03 - Space		
	UBRR1H = 0;
  	UBRR1L = baud;   																		// ������������� �������� ������ �� ����������������� ����������	
//  	UCSR1C |= (1<<URSEL1)|(1<<UCSZ11)|(1<<UCSZ10);											// 8-���
  	
  	switch (eepromRd(1))	
  	{
  		case 0:	// �������� �������� ���
  		{
  			tmp &= ~((1<<UPM11)|(1<<UPM10));
  			break;
  		}
  		case 1: // �������� �������� ����
  		{
  			// ����� ��������
  			switch (eepromRd(3))
  			{
  				case 0:	// Odd
  				{
  					tmp |= (1<<UPM11)|(1<<UPM10);
  					break;
  				}
  				case 1: // Even
  				{
  					tmp |= (1<<UPM11);
  					tmp &= ~(1<<UPM10);
  					break;
  				}
  				default: // ��� ��������
  				{
  					tmp &= ~((1<<UPM11)|(1<<UPM10));
  					break;
  				}
  			}
  		}
  		default: // ��� �������� ��������
  		{
  			tmp &= ~((1<<UPM11)|(1<<UPM10));
  			break;
  		}
  	}

  	// ���������� ���� �����
  	if (eepromRd(2) == 0x02)
  		tmp |= (1<<USBS1);	// 2 �����
  	else
  		tmp &= ~(1<<USBS1);	// 1 ����

  	tmp |= (1<<URSEL1)|(1<<UCSZ11)|(1<<UCSZ10);											// 8-���
  	UCSR1C |= tmp;
  	UCSR1B |= (1<<RXCIE1)|(1<<RXEN1)|(1<<TXEN1)|(1<<TXCIE1);								// ���������� ������, ��������, ��������� �� ������ � ��������
}


//==========================================================================================
// ���������� �� ������ ������� �� �����
//==========================================================================================
#pragma vector=USART1_RXC_vect 
__interrupt void pcToMcuRecieve(void)
{
	unsigned char c = UDR1;
	if (c == '\r' )
	{
		commandHostToGsmCR_Wr = hostToGsmBuf_Wr;											// ��������� ������ � �������� �������� CR
		flagRecieveCRHostToGsm = 0x01;														// ���������� ����, ��� ������� CR
	}
	TCNT2 = 0x00;																			// ����� �������� �2
	flagTimeInterval = 0x00;																// ����� ����� ������� ���������� ��������� = 3.5 �������
	if (c!= '\n')
		putCharFromHostToGsmBuf(c);															// �������� �������� ������ � ��������� ����� HostToGsmBuf
}


//==========================================================================================
// ���������� �� �������� ������� � ����� 
//==========================================================================================
#pragma vector=USART1_TXC_vect 
__interrupt void UartTransmit1(void)
{
	// ���� ������� ����� �������� �� Gsm � �����
	if (usartGsmToHostState == 0x00)														
	{
		if (gsmToHostBuf_Wr != gsmToHostBuf_Rd) 											// ���� � ������ ���� ������ ��� ��������                                 
			UDR1 = getCharFromGsmToHostBuf();                                               // ���������� ��������� ���� �� ��������                              
		else
		{  
			flagGsmToHostTxEnable = 0x00;                                             		// ���������� ���� ������ �������� �� ������.
			//REDE = 0;                                                            			// ����� ����������� �� �����, RTS = 1
		}
	}
	// ���� ������� ����� �������� �������� �����
	if (usartGsmToHostState == 0x01)
	{
		if (toHostBuf_Wr != toHostBuf_Rd)
		{
			UDR1 = *toHostBuf_Rd;                                         					// �������� ��������
			toHostBuf_Rd +=1;
		}
		else
		{
			flagGsmToHostTxEnable = 0x00;                                             		// ���������� ���� ������ �������� �� ������.
			//REDE = 0;                                                            			// ����� ����������� �� �����, RTS = 1
		}	
	}
}


//==========================================================================================
// ���������� �� ������ ������� �� GSM
//==========================================================================================
#pragma vector=USART0_RXC_vect 
__interrupt void mcuToGsmRecieve(void)
{
	unsigned char c = UDR0;																	// ��������� ����� ������
	// ���� � ������ ������ �� ������� ������ �� ������, �� � ������� ������ ��������
	if ( flagWaitRespFromGsm == 0)
	{
		if (c!= '\b')																		// ���� ��� �� Backspace-������, ��
		putCharFromGsmToHostBuf(c);															// �������� �������� ������ � ��������� ����� GsmToHost
	}
	// ���� ������� ����� �� ������, �� ���������� ��� �������� ������� � ����� ������� �� ������
	else
	{
		*answerFromGsmBuf_Wr = c;															// ���������� ����� ������ � ����� ������� �� ������
		answerFromGsmBuf_Wr +=1;
		timer0cnt = 0x00;																	// ���������� ������ ������� ��������
	}
}


//==========================================================================================
// ���������� �� �������� ������� � GSM
//==========================================================================================
#pragma vector=USART0_TXC_vect 
__interrupt void UartTransmit0(void)
{
	// ���� ����������� ����� �������� �� ����� � Gsm
	if (usartHostToGsmState == 0x00)														// ���� ����������� ����� �������� �� ����� � Gsm
	{
		// ���� ��� ������ ������ \r �� �����
		if (flagRecieveCRHostToGsm) 														
		{
			//�� ������� �� ������� CR � ���������� ��������
			if (commandHostToGsmCR_Wr != hostToGsmBuf_Rd)									// ���� �� ����� �� CR ���������� ��� ��� ����
				UDR0 = getCharFromHostToGsmBuf();
			else
				flagHostToGsmTxEnable = 0x00; 												// ��� ����� ���������� ��������
		}
		// ���� ������ \r �� ��������
		else
		{
			// �������� ������ � ������� ������ �� ����� � Gsm
			if (hostToGsmBuf_Wr != hostToGsmBuf_Rd) 										// ���� � ������ ���� ������ ��� ��������                               
		    	UDR0 = getCharFromHostToGsmBuf();                                        		// ���������� ��������� ���� �� ��������                               
			else
				flagHostToGsmTxEnable = 0x00;                                            		// ���������� ���� ������ �������� �� ������.
		}
	}	
	// ���� ������� ����� �������� �������� � Gsm
	if (usartHostToGsmState == 0x01)
	{
		if (toGsmBuf_Wr != toGsmBuf_Rd)
		{
			UDR0 = *toGsmBuf_Rd;                                         					// �������� ��������
			toGsmBuf_Rd +=1;
		}
		else
			flagHostToGsmTxEnable = 0x00;                                             		// ���������� ���� ������ �������� �� ������.
	}
}


//==========================================================================================
// �������� ������ � ����� �������� �� ����� � GSM (����� �� UART1)
//==========================================================================================
void putCharFromHostToGsmBuf (unsigned char c)
{
	*hostToGsmBuf_Wr = c;                                                  					// ���������� ������ ���� � ������, ����� ������� �������� ���������.
    if (hostToGsmBuf_Wr == ((unsigned char *)&hostToGsmBuf) + SIZEOFRECIEVEBUF - 1)			// ���� �������� ����� ������
        hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;                            		// ����������� ��������� ����� ������ ������.
    else 
    	hostToGsmBuf_Wr +=1;                                                       			// ����� ��������� �� ��������� ������ ������
}


//==========================================================================================
// ����� ������ �� ������ �������� �� GSM � ����� (�������� �� UART1)
//==========================================================================================
unsigned char getCharFromGsmToHostBuf (void)
{
	unsigned char c = *gsmToHostBuf_Rd;                                                 	// ����� ������ �� ������
    if (gsmToHostBuf_Rd == ((unsigned char *)&gsmToHostBuf) + SIZEOFRECIEVEBUF - 1)			// ���� �������� ����� ������
        gsmToHostBuf_Rd = (unsigned char *)&gsmToHostBuf;                            		// ����������� ��������� ����� ������ ������.
    else 
    	gsmToHostBuf_Rd +=1;                                                       			// ����� ��������� �� ��������� ������ ������
    return c;
}


//==========================================================================================
// �������� ������ � ����� �������� �� GSM � ����� (����� �� UART0)
//==========================================================================================
void putCharFromGsmToHostBuf (unsigned char c)
{
	*gsmToHostBuf_Wr = c;                                                  					// ���������� ������ ���� � ������, ����� ������� �������� ���������.
    if (gsmToHostBuf_Wr == ((unsigned char *)&gsmToHostBuf) + SIZEOFRECIEVEBUF - 1)			// ���� �������� ����� ������
        gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;                            		// ����������� ��������� ����� ������ ������.
    else 
    	gsmToHostBuf_Wr +=1;                                                       			// ����� ��������� �� ��������� ������ ������
}


//==========================================================================================
// ����� ������ �� ������ �������� �� ����� � GSM (�������� �� UART0)
//==========================================================================================
unsigned char getCharFromHostToGsmBuf (void)
{
	unsigned char c = *hostToGsmBuf_Rd;                                                 	// ����� ������ �� ������
    if (hostToGsmBuf_Rd == ((unsigned char *)&hostToGsmBuf) + SIZEOFRECIEVEBUF - 1)			// ���� �������� ����� ������
        hostToGsmBuf_Rd = (unsigned char *)&hostToGsmBuf;                            		// ����������� ��������� ����� ������ ������.
    else 
    	hostToGsmBuf_Rd +=1;                                                       			// ����� ��������� �� ��������� ������ ������
    return c;
}


//==========================================================================================
// ��������� ������ �������� �����
//==========================================================================================
void printfToHost (char *string)															
{
	while (flagGsmToHostTxEnable != 0x00)													// ���������, ���� �� ���������� ���������� ����� ���������� �������
		;
	usartGsmToHostState = 0x01;																// ������������ ����� �������� �������� � �����
	toHostBuf_Wr = toHostBuf_Rd = (unsigned char *)&toHostBuf;								// ������������ ��������� � ������ ������
	while (*string != '\0')																	// ���������� ������ � �����
	{
		*toHostBuf_Wr = *string;
		string +=1;
		toHostBuf_Wr +=1;
	}
	while ( !(UCSR1A & (1<<UDRE1)) ); 														// ���� ���� ������������ ���������������� ����1
		;
	//REDE = 1;																				// ����������� ���������������� �� ��������
	flagGsmToHostTxEnable = 0x01;															// ������������� ���� ������ �������� � �����
	UDR1 = *toHostBuf_Rd;                                         							// �������� ��������
	toHostBuf_Rd +=1;
	while (flagGsmToHostTxEnable != 0x00)													// ���� ���� �������� �� ����������
		;
	usartGsmToHostState = 0x00;																// ���������� ����� - �������� �� Gsm � �����
}


//==========================================================================================
// ��������� ������ �������� Gsm
//==========================================================================================
void printfToGsm (char *string)
{
	while (flagHostToGsmTxEnable != 0x00)													// ���������, ���� �� ���������� ���������� ����� ���������� �������
		;
	usartHostToGsmState = 0x01;																// ������������ ����� �������� �������� � �����
	toGsmBuf_Wr = toGsmBuf_Rd = (unsigned char *)&toGsmBuf;									// ������������ ��������� � ������ ������
	while (*string != '\0')																	// ���������� ������ � �����
	{
		*toGsmBuf_Wr = *string;
		string +=1;
		toGsmBuf_Wr +=1;
	}
	while ( !(UCSR0A & (1<<UDRE0)) ); 														// ���� ���� ������������ ���������������� ����1
		;
	flagHostToGsmTxEnable = 0x01;															// ������������� ���� ������ �������� � �����
	UDR0 = *toGsmBuf_Rd;                                         							// �������� ��������
	toGsmBuf_Rd +=1;
	while (flagHostToGsmTxEnable != 0x00)													// ���� ���� �������� �� ����������
		;
	usartHostToGsmState = 0x00;																// ���������� ����� - �������� �� Gsm � �����
}


//==========================================================================================
// ������������ ���������� ����� ������
//==========================================================================================
unsigned char strlen (char *string)
{
	unsigned char n;
	for (n = 0; *string != '\0'; string++)
		n++;
	return n;
}


//==========================================================================================
// ����� ������� � ������� �� �����
//==========================================================================================
unsigned char findCommandToGsm (char *string)												// ������������ ������ ������� � ������� �� ����� � Gsm, 
																							// ���������� ��������� ��������� 1 - �� ���������, 0 - ��������� 
{
	unsigned char cnt = 0;																	// ������� �������� ������
	unsigned char *txTmp;																	// �������������� ��������� �� ������� �������� ������ �� �����
	unsigned char c = 0;

	while (*string != '\0')																	// ������� ���������� �������� � ������
	{
		cnt += 1;																			// +1 � �������� ��������
		string += 1;																		// �������������� ��������� 
	}

	txTmp = commandHostToGsmCR_Wr;															// ������ ��������� �� ������ \r
	string -= 1;																			// ����������� '/0'
	while (cnt > 0)																			// ���� �� ���������� ��� ������� � ������
	{
		c = *txTmp;
		if((c > 0x60) && (c < 0x7B))														// ���� ������ � �������� ��������, �� ��������� ��� � ���������
			c = c - 0x20; 																	// ������������ � ������ �������� ������ ���������
		if((*string) != (c))																// ���������� ������� �� ������� ������� � ����� ������
			return 1;																		// ������ ���� - ������ �� ���������, ������� ���������� 1
		cnt -=1;																			// ��������� �� ���������� ������ � ������
		string -=1;

		if(txTmp == (unsigned char *)&hostToGsmBuf)											// ���� ��� ��������� ��� � ������ ������
			txTmp = (((unsigned char *)&hostToGsmBuf) + SIZEOFRECIEVEBUF - 1);				// ������� ��� � ����� ������
		else 
			txTmp -=1;																		// ���� ������ -1
	}
	return 0;																				// ���� ������ �������, �� ������� ���������� 0
}


//==========================================================================================
// �������� ������ �� GSM, ���������� ���������, ��������� ����� � �������� ��� ���
//==========================================================================================
unsigned char waitRespFromGsm (char *string, char *string1, unsigned char timeout)
{
	answerFromGsmBuf_Wr = answerFromGsmBuf_Rd = (unsigned char *)&answerFromGsmBuf[SIZEOFRECIEVEBUF];	// ����������� ���������� ����� ������ ������ ������� �� ������
	flagWaitRespFromGsm = 0x01;																// ���������� ����, ��� ������ ������� ������ �� ������
	timeoutRespFromGsm = timeout;															// ���������� ����� �������� ������ �� ������
	timer0cnt = 0x00;																		// �������� ������ �������
	flagTimeoutRespFromGsmOVR = 0x00;														// �������� ���� ����������� ��������
	while (flagTimeoutRespFromGsmOVR == 0x00)												// ���� ����� ������ �������
		;
	flagWaitRespFromGsm = 0x00;																// ����� �� ���� ������ �� ������
	/*	
	if (parsRespFromGsm(string) == 0)
		return 0;
	else
		return 1;
		*/
	if (parsRespFromGsm(string) == 0)
		return 0;
	else
		if (parsRespFromGsm(string1) == 0)
			return 0;
		else
			return 1;
}


//==========================================================================================
// ������ ������ �� Gsm ������, ���� ����� ������, �� ���������� 0, � ��������� ������ 1
//==========================================================================================
unsigned char parsRespFromGsm (char *string)
{
	unsigned char cnt = 0;																	// ������� �������� ������
	unsigned char *rxTmp;																	// �������������� ��������� �� ������� �������� ������ �� Gsm

	while (*string != '\0')																	// ������� ���������� �������� � ������
	{
		cnt += 1;																			// +1 � �������� ��������
		string += 1;																		// �������������� ��������� 
	}

	rxTmp = answerFromGsmBuf_Wr - 1;														// ������ ��������� �� ������� �������� ������
	string -= 1;																			// ����������� '/0'
	while (cnt > 0)																			// ���� �� ���������� ��� ������� � ������
	{
		if((*string) != (*rxTmp))																// ���������� ������� �� ������� ������� � ����� ������
			return 1;																		// ������ ���� - ������ �� ���������, ������� ���������� 1
		cnt -=1;																			// ��������� �� ���������� ������ � ������
		string -=1;
		rxTmp -=1;																			// ������ -1
	}
	return 0;																				// ���� ������ �������, �� ������� ���������� 0
}

void diagnosticMessage (char *string)														// ��������������� ���������
{
	if (diagMode)																			// ���� ��������������� ����� �����������, �� ��������� ���������
	{
		printfToHost(string);
	}
}


//
// __flash
//


//==========================================================================================
// ��������� ������ �������� �����
//==========================================================================================
void printfToHostFl (char __flash *string)															
{
	while (flagGsmToHostTxEnable != 0x00)													// ���������, ���� �� ���������� ���������� ����� ���������� �������
		;
	usartGsmToHostState = 0x01;																// ������������ ����� �������� �������� � �����
	toHostBuf_Wr = toHostBuf_Rd = (unsigned char *)&toHostBuf;								// ������������ ��������� � ������ ������
	while (*string != '\0')																	// ���������� ������ � �����
	{
		*toHostBuf_Wr = *string;
		string +=1;
		toHostBuf_Wr +=1;
	}
	while ( !(UCSR1A & (1<<UDRE1)) ); 														// ���� ���� ������������ ���������������� ����1
		;
	//REDE = 1;																				// ����������� ���������������� �� ��������
	flagGsmToHostTxEnable = 0x01;															// ������������� ���� ������ �������� � �����
	UDR1 = *toHostBuf_Rd;                                         							// �������� ��������
	toHostBuf_Rd +=1;
	while (flagGsmToHostTxEnable != 0x00)													// ���� ���� �������� �� ����������
		;
	usartGsmToHostState = 0x00;																// ���������� ����� - �������� �� Gsm � �����
}

//==========================================================================================
// ��������� ������ �������� Gsm
//==========================================================================================
void printfToGsmFl (char __flash *string)
{
	while (flagHostToGsmTxEnable != 0x00)													// ���������, ���� �� ���������� ���������� ����� ���������� �������
		;
	usartHostToGsmState = 0x01;																// ������������ ����� �������� �������� � �����
	toGsmBuf_Wr = toGsmBuf_Rd = (unsigned char *)&toGsmBuf;									// ������������ ��������� � ������ ������
	while (*string != '\0')																	// ���������� ������ � �����
	{
		*toGsmBuf_Wr = *string;
		string +=1;
		toGsmBuf_Wr +=1;
	}
	while ( !(UCSR0A & (1<<UDRE0)) ); 														// ���� ���� ������������ ���������������� ����1
		;
	flagHostToGsmTxEnable = 0x01;															// ������������� ���� ������ �������� � �����
	UDR0 = *toGsmBuf_Rd;                                         							// �������� ��������
	toGsmBuf_Rd +=1;
	while (flagHostToGsmTxEnable != 0x00)													// ���� ���� �������� �� ����������
		;
	usartHostToGsmState = 0x00;																// ���������� ����� - �������� �� Gsm � �����
}

//==========================================================================================
// ����� ������� � ������� �� �����
//==========================================================================================
unsigned char findCommandToGsmFl (char __flash *string)										// ������������ ������ ������� � ������� �� ����� � Gsm, 
																							// ���������� ��������� ��������� 1 - �� ���������, 0 - ��������� 
{
	unsigned char cnt = 0;																	// ������� �������� ������
	unsigned char *txTmp;																	// �������������� ��������� �� ������� �������� ������ �� �����
	unsigned char c = 0;

	while (*string != '\0')																	// ������� ���������� �������� � ������
	{
		cnt += 1;																			// +1 � �������� ��������
		string += 1;																		// �������������� ��������� 
	}

	txTmp = commandHostToGsmCR_Wr;															// ������ ��������� �� ������ \r
	string -= 1;																			// ����������� '/0'
	while (cnt > 0)																			// ���� �� ���������� ��� ������� � ������
	{
		c = *txTmp;
		if((c > 0x60) && (c < 0x7B))														// ���� ������ � �������� ��������, �� ��������� ��� � ���������
			c = c - 0x20; 																	// ������������ � ������ �������� ������ ���������
		if((*string) != (c))																// ���������� ������� �� ������� ������� � ����� ������
			return 1;																		// ������ ���� - ������ �� ���������, ������� ���������� 1
		cnt -=1;																			// ��������� �� ���������� ������ � ������
		string -=1;

		if(txTmp == (unsigned char *)&hostToGsmBuf)											// ���� ��� ��������� ��� � ������ ������
			txTmp = (((unsigned char *)&hostToGsmBuf) + SIZEOFRECIEVEBUF - 1);				// ������� ��� � ����� ������
		else 
			txTmp -=1;																		// ���� ������ -1
	}
	return 0;																				// ���� ������ �������, �� ������� ���������� 0
}


//==========================================================================================
// �������� ������ �� GSM, ���������� ���������, ��������� ����� � �������� ��� ���
//==========================================================================================
unsigned char waitRespFromGsmFl (char __flash *string, char __flash *string1, unsigned char timeout)
{
	answerFromGsmBuf_Wr = answerFromGsmBuf_Rd = (unsigned char *)&answerFromGsmBuf[SIZEOFRECIEVEBUF];	// ����������� ���������� ����� ������ ������ ������� �� ������
	flagWaitRespFromGsm = 0x01;																// ���������� ����, ��� ������ ������� ������ �� ������
	timeoutRespFromGsm = timeout;															// ���������� ����� �������� ������ �� ������
	timer0cnt = 0x00;																		// �������� ������ �������
	flagTimeoutRespFromGsmOVR = 0x00;														// �������� ���� ����������� ��������
	while (flagTimeoutRespFromGsmOVR == 0x00)												// ���� ����� ������ �������
		;
	flagWaitRespFromGsm = 0x00;																// ����� �� ���� ������ �� ������
	/*	
	if (parsRespFromGsm(string) == 0)
		return 0;
	else
		return 1;
		*/
	if (parsRespFromGsmFl(string) == 0)
		return 0;
	else
		if (parsRespFromGsmFl(string1) == 0)
			return 0;
		else
			return 1;
}


//==========================================================================================
// ������ ������ �� Gsm ������, ���� ����� ������, �� ���������� 0, � ��������� ������ 1
//==========================================================================================
unsigned char parsRespFromGsmFl (char __flash *string)
{
	unsigned char cnt = 0;																	// ������� �������� ������
	unsigned char *rxTmp;																	// �������������� ��������� �� ������� �������� ������ �� Gsm

	while (*string != '\0')																	// ������� ���������� �������� � ������
	{
		cnt += 1;																			// +1 � �������� ��������
		string += 1;																		// �������������� ��������� 
	}

	rxTmp = answerFromGsmBuf_Wr - 1;														// ������ ��������� �� ������� �������� ������
	string -= 1;																			// ����������� '/0'
	while (cnt > 0)																			// ���� �� ���������� ��� ������� � ������
	{
		if((*string) != (*rxTmp))																// ���������� ������� �� ������� ������� � ����� ������
			return 1;																		// ������ ���� - ������ �� ���������, ������� ���������� 1
		cnt -=1;																			// ��������� �� ���������� ������ � ������
		string -=1;
		rxTmp -=1;																			// ������ -1
	}
	return 0;																				// ���� ������ �������, �� ������� ���������� 0
}