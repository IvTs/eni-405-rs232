/*
* ������ GSM/GPRS ������ � RS-485 ����������� 
* � ���������� ������ ������� 220�->4�
*
* *
* MCU - ATmega162
* GSM/GPRS - SIM900
* RS-232 SP3238
* Crystal Freq - 7.3728 MHz
* 
* ����������: ������ �.�.
* �����������: ��� "�������-��������"
* ���� ������: 18.05.2018
* ���� ����������: 
*/

#include <ioavr.h>
#include <intrinsics.h>

#define REDE 			PORTB_Bit4 															// ������������ �����-�������� �� RS-485 REDE=1 �������� REDE=0 �����
#define PWR_KEY			PORTA_Bit0															// ����� ���/���� ������, ������� �� ����� �� 1.2 �������, ����� ���/���� ������, ����� ���������� �������� � +
#define NRESET  		PORTA_Bit1															// 
#define STATUS 			PINA_Bit2															// ����� ��� �������� ��������� ������. ����� ���������, �� ������� �� �����������, ���� ������ �� ����������� � 1
																							// ����� PowerDown ��������� ������������ � 0
#define DCD 			PIND_Bit5   														// ������� ��� ����� �� - ������ SIM900
#define RTS 			PORTD_Bit2  														//
#define CTS 			PIND_Bit3   														//
#define RING			PIND_Bit4  															//
#define DTR 			PORTD_Bit6  														//

#define HOUR 			1
#define DAY 			24
#define WEEK 			168
#define MONTH 			720

#define TIMER1START {timer1cnt = 0;  timer1Ovf = 0; TCCR1B|=(1<<CS12)|(1<<CS10);}//|(1<<CS11);}
#define TIMER1STOP TCCR1B = 0



//----������������--------------------------------------------------------------------------
void powerUpModule(void);																	// ��������� ������
void timer2Ini(unsigned char baud);															// ������������� ������� ��� ������� ��������� ���������� ������ 3.5 �������	
void timer0Ini(void);																		// ������������� ������� ��� ������� ��������� ������ �� ������ Gsm
void timer1Ini(void);																		// ������ ������� ��� ������������ ������
//unsigned char moduleInit(void);															// ������������� ������, ��������������� ��������
unsigned char eepromWr (unsigned char AddrH, unsigned char Data);							// ������ � ������
unsigned char eepromRd (unsigned char AddrH);												// ������ �� ������
void setResetTimer(void);																	// ��������� ������� ������������ ������					

//----����������----------------------------------------------------------------------------
unsigned char timer0cnt = 0x00;
int cntT0 = 0x00;
unsigned char flagCntT0Ovf = 0x00;
unsigned char modulIni = 0x00;
int timer1cnt = 0;																			// ������� ����������, +1 1 ��� � 9 ���, ����� ������� 1 ��� = 400 cnt
int timerTarget = 0x00;
unsigned char timer1Ovf = 0;																

unsigned diagMode = 0x00;																	// ����� �����������
unsigned char baud = 0x03;
unsigned char flagGsmInit = 0x00;															// ����=0 GSM ������ ��� �� �������
unsigned char atv = 0x01;																	// ����� �� ������ � ���� ������

//const char AT_RESET[]={"AT+RESET\r"};
char *ptr;



#include "USART.c"
#include "Fuse.c"
//==========================================================================================
//----�������� ���������--------------------------------------------------------------------
//==========================================================================================
void main( void )
{//----��������� ������ �����/������-----------------------------------------------------
	DDRA = 0x00;																			// ��� ����� �� ����
	DDRB = 0x00;
	DDRC = 0x00;                              
	DDRD = 0x00;
	
	// DDRA |= (1<<0) | (1<<1);  																// NRESET, PWR_KEY
	// PORTA |= (1<<2) | (1<<3) | (1<<1) | (1<<0);  											// STATUS, ADC_POWER, PWR_KEY=1, NRESET
	// DDRD |= (1<<2) | (1<<6);  																// RTS_SIM, DTR_SIM
	// PORTD |= (1<<0) |(1<<3) | (1<<4);   													// PULL_UP RX0, CTS_SIM, RING_SIM
	// DDRB |= (1<<4); 																		// RE/DE
	// PORTB |= (1<<2);																		// PULL_UP RX1

	DDRA |= (1<<0)|(1<<1);  // NRESET, PWR_KEY
  	PORTA |= (1<<2)|(1<<3)|(1<<1)|(1<<0);  // STATUS, ADC_POWER, PWR_KEY=1, NRESET
  
  	DDRB_Bit4 = 1; //ONLINE sp3238
  	PORTB_Bit1 = 1;   // Pull-up DTR_SP
  
  	DDRC |= (1<<2)|(1<<3)|(1<<5)|(1<<7);   // DSR_SP, CTS_SP, RING_SP, SHDH sp3238
  	PORTC |= (1<<6);   // RTS_SP
    
  	DDRD |= (1<<2)|(1<<6);  // RTS_SIM, DTR_SIM
  	PORTD |= (1<<3)|(1<<4);   // CTS_SIM, RING_SIM

	PORTC_Bit7 = 1;   // SHDN ����� �� ������� ������ SP3238
  	PORTB_Bit4 = 1;   // ONLINE ���������� ������ SP3238

	REDE = 1; 																				// ���������������� ����������� �� �����

	Fuses ();                             													// �������� ����� ������
	SFuses ();                            													//

	hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;          			// ����������� ���������� ������ ������ ���������� ������
	gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;

	//������������� WatchDog �������
	WDTCR |= (1<<WDCE) | (1<<WDE);
	WDTCR = (1<<WDE) | (1<<WDP2) | (1<<WDP0) | (1<<WDP1);									// 2.2 ���
	__watchdog_reset();
	
	__enable_interrupt();																	// ���������� ��������� ����������

	baud = eepromRd(0);
	if (baud == 0xff)
	{
		baud = BAUD;
		while (eepromWr(1,0));																// ��� ��������
		while (eepromWr(2,1));																// 1 ����
		while (eepromWr(3,3));																// ��� ��������
	}

	

	pcToMcuUartInit(baud);																	// ������������� Uart
	mcuToGsmUartInit(baud);
	timer2Ini(baud);																		// ������������� ������� ��� ������� ��������� ���������� ������ ��������
	timer0Ini();
	timer1Ini();

	setResetTimer();																		// ��������� ������������������� ������
	__watchdog_reset();	
	powerUpModule();																		// ������ ������
	__delay_cycles(5000000);
	__watchdog_reset();
	__delay_cycles(5000000);
	__watchdog_reset();
	__delay_cycles(5000000);
	__watchdog_reset();

 	// printfToHost("EnI-405\r\n");

	//----������ ������������ �����---------------------------------------------------------
	while(1)
	{
		__watchdog_reset();

		if(!flagGsmInit)
		{
			printfToGsm("AT\r\n");
			if ( waitRespFromGsm("\r\nOK\r\n","0\r",50) == 0)
			{
				flagGsmInit = 1;
				printfToHost("\r\nv.01.01.0001\r\n");
				printfToHost("\r\nReady\r\n");

				printfToGsm("AT\r\n");														// ������� ��������� ATV ���������
				if ( waitRespFromGsm("\r\nOK\r\n","\r\nOK\r\n",50) == 0)
					atv = 1;
                printfToGsm("AT\r\n");
				if ( waitRespFromGsm("0\r","0\r",50) == 0)
					atv = 0;
			}
			else
			{
				//----������ ������---------------------------------------------------------------------
				// printfToHost("\r\nERROR\r\n");
				__watchdog_reset();
				PWR_KEY = 0; 																			// �������� � 0
				__delay_cycles(7372800*0.8);															// ���� 1.2 ���
				while(STATUS != 0);																		// ������ ������ �� ��� ����� ����������� � ������ ����������� � 1
				PWR_KEY = 1; 																			// ������� � 1 	
			}
			
			hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         				// ����������� ���������� ������ ������ ���������� ������
			gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
		}
		

		// ���� ��� ������ CR ������ �� ����� 
		if (flagRecieveCRHostToGsm)
		{
			// �������� ������ ������� � ���������� ���������� ������
			// ������� �� ���������� ������ AT+RESET\r
			//ptr = (char *)&AT_RESET;
			if ( findCommandToGsmFl(AT_RESET) == 0)
			{
				for (unsigned char i = 0; i != 10; i++)										// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");
				if(atv)	printfToHostFl(OK);
				else 	printfToHostFl(OK0);
				powerUpModule();
				__delay_cycles(20000000);
			}


			// ������� �� ������ ������ �� ������ POWER DOWN
			else if ( findCommandToGsmFl(AT_START) == 0)
			{
				for (unsigned char i = 0; i != 10; i++)										// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");
				if(atv)	printfToHostFl(OK);
				else	printfToHostFl(OK0);
				powerUpModule();
				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;          			// ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}


			//�������� ���� ��������� ��������������� ����� ������
			else if ( findCommandToGsmFl(AT_DIAGN) == 0)
			{
				for (unsigned char i = 0; i != 10; i++)									// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");
				if( diagMode)																// ���� ��������������� ����� �������, �� ���������
				{
					diagMode = 0;													
					printfToHost("\r\nDiag OFF\r\n");
				}
				else																		// ���� ��������, �� ��������
				{	
					diagMode = 1;
					printfToHost("\r\nDiag ON\r\n");
				}
				printfToGsm("AT\r\n");
				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}


			// ������� �� ����� ��������
			// 9600
			else if (findCommandToGsmFl(AT_IPR9600) == 0)
			{
				for (unsigned char i = 0; i != 13; i++)										// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");
				/*
				printfToGsm("AT+IPR=0\r");
				if (( waitRespFromGsm("\r\nOK\r\n","0\r",50) == 0))
				{
					printfToHost("\r\nOK\r\n");
					while (eepromWr(0,47))													// ���� ���� �� ��������� � ������
						;
					printfToGsm("\r\nAT&W\r\n");
					powerUpModule();														// ���������� ������������� �����
					__delay_cycles(16000000);
				}
				else
				{
					printfToHost("\r\nERROR\r\n");
				}		
				*/
				printfToGsmFl(AT_IPR0);
				if(atv)	printfToHostFl(OK);
				else	printfToHostFl(OK0);
				while (eepromWr(0,47))														// ���� ���� �� ��������� � ������
					;		
				printfToGsmFl(AT_W);
				powerUpModule();															// ���������� ������������� �����
				__delay_cycles(16000000);
				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}


			// 19200
			else if (findCommandToGsmFl(AT_IPR19200) == 0)
			{
				for (unsigned char i = 0; i != 14; i++)										// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");

				/*
				printfToGsm("AT+IPR=0\r");
				if( waitRespFromGsm("\r\nOK\r\n","0\r",50) == 0)
				{
					printfToHost("\r\nOK\r\n");
					while (eepromWr(0,23))													// ���� ���� �� ��������� � ������
						;
					printfToGsm("\r\nAT&W\r\n");
					powerUpModule();														// ���������� ������������� �����
					__delay_cycles(16000000);
				}
				else
				{
					printfToHost("\r\nERROR\r\n");
				}
				*/
				printfToGsmFl(AT_IPR0);
				if(atv)	printfToHostFl(OK);
				else	printfToHostFl(OK0);
				while (eepromWr(0,23))														// ���� ���� �� ��������� � ������
					;		
				printfToGsmFl(AT_W);
				powerUpModule();															// ���������� ������������� �����
				__delay_cycles(16000000);		

				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}


			// 38400
			else if (findCommandToGsmFl(AT_IPR38400) == 0)
			{
				for (unsigned char i = 0; i != 14; i++)				// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");
				/*
				printfToGsm("AT+IPR=0\r");
				if( waitRespFromGsm("\r\nOK\r\n","0\r",50) == 0)
				{
					printfToHost("\r\nOK\r\n");
					while (eepromWr(0,11))													// ���� ���� �� ��������� � ������
						;
					printfToGsm("\r\nAT&W\r\n");
					powerUpModule();														// ���������� ������������� �����
					__delay_cycles(16000000);														// ���������� ������������� �����
				}
				else
				{
					printfToHost("\r\nERROR\r\n");
				}
				*/
				printfToGsmFl(AT_IPR0);
				if(atv)	printfToHostFl(OK);
				else	printfToHostFl(OK0);
				while (eepromWr(0,11))														// ���� ���� �� ��������� � ������
					;		
				printfToGsmFl(AT_W);
				powerUpModule();															// ���������� ������������� �����
				__delay_cycles(16000000);		

				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}


			// 57600
			else if (findCommandToGsmFl(AT_IPR57600) == 0)
			{
				for (unsigned char i = 0; i != 14; i++)				// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");

				/*
				printfToGsm("AT+IPR=0\r");
				if( waitRespFromGsm("\r\nOK\r\n","0\r",50) == 0)
				{
					printfToHost("\r\nOK\r\n");
					while (eepromWr(0,7))													// ���� ���� �� ��������� � ������
						;
					printfToGsm("\r\nAT&W\r\n");
					powerUpModule();														// ���������� ������������� �����
					__delay_cycles(16000000);
				}
				else
				{
					printfToHost("\r\nERROR\r\n");
				}		
				*/
				printfToGsmFl(AT_IPR0);
				if(atv)	printfToHostFl(OK);
				else	printfToHostFl(OK0);
				while (eepromWr(0,7))														// ���� ���� �� ��������� � ������
					;		
				printfToGsmFl(AT_W);
				powerUpModule();															// ���������� ������������� �����
				__delay_cycles(16000000);
				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}


			// 115200
			else if (findCommandToGsmFl(AT_IPR115200) == 0)
			{
				for (unsigned char i = 0; i != 15; i++)										// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");

				/*
				printfToGsm("AT+IPR=0\r");
				if( waitRespFromGsm("\r\nOK\r\n","0\r",50) == 0)
				{
					printfToHost("\r\nOK\r\n");
					while (eepromWr(0,3))													// ���� ���� �� ��������� � ������
						;
					printfToGsm("\r\nAT&W\r\n");
					powerUpModule();														// ���������� ������������� �����
					__delay_cycles(16000000);
				}
				else
				{
					printfToHost("\r\nERROR\r\n");
				}
				*/
				printfToGsmFl(AT_IPR0);
				if(atv)	printfToHostFl(OK);
				else	printfToHostFl(OK0);
				while (eepromWr(0,3))														// ���� ���� �� ��������� � ������
					;		
				printfToGsmFl(AT_W);
				powerUpModule();															// ���������� ������������� �����
				__delay_cycles(16000000);

				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}


			// ������� �� ����� ��������
			// 8 data 0 parity 2 stop 
			else if ((findCommandToGsmFl(AT_ICF10) == 0) || (findCommandToGsmFl(AT_ICF11) == 0) || (findCommandToGsmFl(AT_ICF13) == 0))
			{
				for (unsigned char i = 0; i != 12; i++)									// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");
				
				while (eepromWr(1,0))													// ���-�� ���������
						;
				while (eepromWr(2,2))													// ���-�� ������
						;
				while (eepromWr(3,3))													// 0 - Odd, 1 - even, 3 - space
						;	
				if(atv)	printfToHostFl(OK);
				else	printfToHostFl(OK0);
				powerUpModule();														// ���������� ������������� �����
				__delay_cycles(16000000);

				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}


			// 8 data 1 parity (odd) 1 stop 
			else if (findCommandToGsmFl(AT_ICF20) == 0)
			{
				for (unsigned char i = 0; i != 12; i++)				// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");
				
				while (eepromWr(1,1))													// ���-�� ���������
						;
				while (eepromWr(2,1))													// ���-�� ������
						;
				while (eepromWr(3,0))													// 0 - Odd
						;	
				if(atv)	printfToHostFl(OK);
				else	printfToHostFl(OK0);
				powerUpModule();														// ���������� ������������� �����
				__delay_cycles(16000000);

				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}


			// 8 data 1 parity (even) 1 stop 
			else if (findCommandToGsmFl(AT_ICF21) == 0)
			{
				for (unsigned char i = 0; i != 12; i++)				// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");
				
				while (eepromWr(1,1))													// ���-�� ���������
						;
				while (eepromWr(2,1))													// ���-�� ������
						;
				while (eepromWr(3,1))													// 1 - even
						;	
				if(atv)	printfToHostFl(OK);
				else	printfToHostFl(OK0);
				powerUpModule();														// ���������� ������������� �����
				__delay_cycles(16000000);													// ���������� ������������� �����

				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}


			// 8 data 1 parity (space) 1 stop 
			else if (findCommandToGsmFl(AT_ICF23) == 0)
			{
				for (unsigned char i = 0; i != 12; i++)				// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");
				
				while (eepromWr(1,1))													// ���-�� ���������
						;
				while (eepromWr(2,1))													// ���-�� ������
						;
				while (eepromWr(3,3))													// 3 - space
						;	
				if(atv)	printfToHostFl(OK);
				else	printfToHostFl(OK0);
				powerUpModule();														// ���������� ������������� �����
				__delay_cycles(16000000);

				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;     // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}


			// 8 data 0 parity 1 stop 
			else if ((findCommandToGsmFl(AT_ICF30) == 0) || (findCommandToGsmFl(AT_ICF31) == 0) || (findCommandToGsmFl(AT_ICF33) == 0))
			{
				for (unsigned char i = 0; i != 12; i++)									// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");
				
				while (eepromWr(1,0))													// ���-�� ���������
						;
				while (eepromWr(2,1))													// ���-�� ������
						;
				while (eepromWr(3,3))													// 0 - Odd, 1 - even, 3 - space
						;	
				if(atv)	printfToHostFl(OK);
				else	printfToHostFl(OK0);
				powerUpModule();														// ���������� ������������� �����
				__delay_cycles(16000000);

				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}

			// ��������� ��������� �������� AT+ICF? 
			else if (findCommandToGsmFl(AT_ICF_) == 0)
			{
				for (unsigned char i = 0; i != 10; i++)									// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");
				// 0 ������ ������ - �������� �� UART 			03 - 115200
				//									  			07 - 57600
				//									  			11 - 38400
				//									  			23 - 19200
				//									  			47 - 9600
				//
				// 1 ������ ������ - ������� ���� �������� 		00 - ��� �������� ��������
				// 										   		01 - ���� �������� ��������
				//
				// 2 ������ ������ - ���������� ����-�����		01 - 1 ���� ���
				//												02 - 2 ���� ����
				//
				// 3 ������ ������ - �������� 					00 - Odd
				//												01 - Even
				//  											03 - Space
				__delay_cycles(737280);
				if (usartGsmToHostState == 0x00)													// E��� ����������� ����� �������� �� Gsm � �����
				{
				// ���� � ������ ���� ������, ������� ���������� ��������� �� GSM � �����
				// ���� �������� ��� �� ������ � � ������� ������ �������� ������� �� ������ ��� ���������� ��������� = 3,5 �������
					if ((gsmToHostBuf_Wr != gsmToHostBuf_Rd) && (flagGsmToHostTxEnable == 0) && (flagTimeInterval == 0x01))	
					{
						while( !(UCSR1A & (1<<UDRE1)) ); 											// ���� ���� ������������ ���������������� ����1
						//REDE = 1;																	// ����������� ���������������� �� ��������
						flagGsmToHostTxEnable = 0x01;												// ������������� ���� ������ �������� �� ����� � GSM
				    	UDR1 = getCharFromGsmToHostBuf();                                         	// �������� ��������
					}
				}

				switch (eepromRd(2))
				{
					case 1:
					{
						switch (eepromRd(3))
						{
							case 0:
							{
								printfToHostFl(AT_ICF20_);
								break;
							}
							case 1:
							{
								printfToHostFl(AT_ICF21_);
								break;
							}
							case 3:
							{
								printfToHostFl(AT_ICF33_);
								break;
							}
							default:
							{
								printfToHostFl(AT_ICF33_);
								break;
							}
						}
						break;
					}
					case 2:
					{
						printfToHostFl(AT_ICF13_);
						break;
					}
					default:
					{
						printfToHostFl(AT_ICF33_);
						break;
					}
				}
				if(atv)	printfToHostFl(OK);
				else	printfToHostFl(OK0);

				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}


			// ��������� �������� ������� ������������ ������
			// - ��������� ������ ������������
			else if (findCommandToGsmFl(AT_TIMER_0) == 0)
			{
				for (unsigned char i = 0; i != 12; i++)										// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");
				TIMER1STOP;																	// ��������� �������				
				while (eepromWr(4,0))														// ������ ��������
						;
				if(atv)	printfToHostFl(OK);
				else	printfToHostFl(OK0);

				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}


			// - ������ ������������ �� 1 ���
			else if (findCommandToGsmFl(AT_TIMER_1) == 0)
			{
				for (unsigned char i = 0; i != 12; i++)										// �������� �������� �������
					printfToGsm("\b");
				timerTarget = HOUR;															// ���� ������� �� 1 ����
				TIMER1START;																// ����� �������
				while (eepromWr(4,1))														// ������ �� ���
						;
				if(atv)	printfToHostFl(OK);
				else	printfToHostFl(OK0);

				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}


			// - ������ ������������ �� 24 ����
			else if (findCommandToGsmFl(AT_TIMER_2) == 0)
			{
				for (unsigned char i = 0; i != 12; i++)										// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");
				timerTarget = DAY;															// ���� ������� 
				TIMER1START;																// ����� �������
				while (eepromWr(4,2))														// ������ �� �����
						;
				if(atv)	printfToHostFl(OK);
				else	printfToHostFl(OK0);

				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}


			// - ������ ������������ �� ������
			else if (findCommandToGsmFl(AT_TIMER_3) == 0)
			{
				for (unsigned char i = 0; i != 12; i++)										// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");
				timerTarget = WEEK;															// ���� ������� �� 1 ������				
				TIMER1START;																// ����� �������				
				while (eepromWr(4,3))														// ������ �� ������
						;
				if(atv)	printfToHostFl(OK);
				else	printfToHostFl(OK0);

				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}


			// - ������ ������������ �� 1 �����
			else if (findCommandToGsmFl(AT_TIMER_4) == 0)
			{
				for (unsigned char i = 0; i != 12; i++)										// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");
				timerTarget = MONTH;														// ���� ������� �� 1 �����
				TIMER1START;																// ����� �������				
				while (eepromWr(4,4))														// ������ �� �����
						;
				if(atv)	printfToHostFl(OK);
				else	printfToHostFl(OK0);

				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}

			// - ������ ������������ �� 1 �����
			else if (findCommandToGsmFl(AT_TIMER_) == 0)
			{
				for (unsigned char i = 0; i != 11; i++)										// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");

				__delay_cycles(737280);
				if (usartGsmToHostState == 0x00)													// E��� ����������� ����� �������� �� Gsm � �����
				{
				// ���� � ������ ���� ������, ������� ���������� ��������� �� GSM � �����
				// ���� �������� ��� �� ������ � � ������� ������ �������� ������� �� ������ ��� ���������� ��������� = 3,5 �������
					if ((gsmToHostBuf_Wr != gsmToHostBuf_Rd) && (flagGsmToHostTxEnable == 0) && (flagTimeInterval == 0x01))	
					{
						while( !(UCSR1A & (1<<UDRE1)) ); 											// ���� ���� ������������ ���������������� ����1
						//REDE = 1;																	// ����������� ���������������� �� ��������
						flagGsmToHostTxEnable = 0x01;												// ������������� ���� ������ �������� �� ����� � GSM
				    	UDR1 = getCharFromGsmToHostBuf();                                         	// �������� ��������
					}
				}

				switch (eepromRd(4))
				{
					case 0:
					{
						printfToHostFl(AT_TIMER0);									// ������ ������������ ��������
						break;
					}
					case 1:
					{
						printfToHostFl(AT_TIMER1);									// 1 ���
						break;
					}
					case 2:
					{
						printfToHostFl(AT_TIMER2);									// �����
						break;
					}
					case 3:
					{
						printfToHostFl(AT_TIMER3);									// ������
						break;
					}
					case 4:
					{
						printfToHostFl(AT_TIMER4);									// �����
						break;
					}
					default:
					{
						printfToHostFl(AT_TIMER0);									// 1 ���
						break;
					}
				}

				if(atv)	printfToHostFl(OK);
				else	printfToHostFl(OK0);

				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}


			// ������ ����������� ������, �� �� ������ ����� �������� ERROR
			// ����� ������� AT+IFC=<>,<> - ���������� �������� �������
			else if ( (findCommandToGsmFl(AT_IFC00) == 0) || (findCommandToGsmFl(AT_IFC01) == 0) || (findCommandToGsmFl(AT_IFC02) == 0) || \
				(findCommandToGsmFl(AT_IFC10) == 0) || (findCommandToGsmFl(AT_IFC11) == 0) || (findCommandToGsmFl(AT_IFC12) == 0) || \
				(findCommandToGsmFl(AT_IFC20) == 0) || (findCommandToGsmFl(AT_IFC21) == 0) || (findCommandToGsmFl(AT_IFC22) == 0) )
			{
				for (unsigned char i = 0; i != 12; i++)				// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");
				if(atv)	printfToHostFl(ERROR);
				else	printfToHostFl(ERROR0);
				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}

			// ����� ������� AT+ICF=<>,<> - ������ �����, �������, ���� ����
			else if ( (findCommandToGsmFl(AT_ICF40) == 0) || (findCommandToGsmFl(AT_ICF41) == 0) || (findCommandToGsmFl(AT_ICF43) == 0) || \
				(findCommandToGsmFl(AT_ICF50) == 0) || (findCommandToGsmFl(AT_ICF51) == 0) || (findCommandToGsmFl(AT_ICF53) == 0) || \
				(findCommandToGsmFl(AT_ICF60) == 0) || (findCommandToGsmFl(AT_ICF61) == 0) || (findCommandToGsmFl(AT_ICF63) == 0) )
			{
				for (unsigned char i = 0; i != 12; i++)				// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");
				if(atv)	printfToHostFl(ERROR);
				else	printfToHostFl(ERROR0);
				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}

			// ������� AT+IPR=<>,<> - �������� ������ �� UART
			else if ( (findCommandToGsmFl(AT_IPR1200) == 0) || (findCommandToGsmFl(AT_IPR2400) == 0) || (findCommandToGsmFl(AT_IPR4800) == 0))				
			{
				for (unsigned char i = 0; i != 12; i++)				// �������� �������� �������
					printfToGsm("\b");
				printfToGsm("\r");
				if(atv)	printfToHostFl(ERROR);
				else	printfToHostFl(ERROR0);
				hostToGsmBuf_Rd = hostToGsmBuf_Wr = (unsigned char *)&hostToGsmBuf;         // ����������� ���������� ������ ������ ���������� ������
				gsmToHostBuf_Rd = gsmToHostBuf_Wr = (unsigned char *)&gsmToHostBuf;
			}

			// ������� ��V0 ��������� ������ ������ ������ � ���� ����� 0-9
			else if (findCommandToGsmFl(ATV0) == 0)
			{
				atv = 0;
			}

			// ������� ��V1 ��������� ������ ������ ������ � ���� ������
			else if (findCommandToGsmFl(ATV1) == 0)
			{
				atv = 1;
			}


			// ���� �� ����� ������� �� �������
			else
				diagnosticMessage("Not found\r\n");
			
			
			flagRecieveCRHostToGsm = 0x00; 													// ���������� ��� �������, ������� ���� �������� CR, ��������� �������� � Gsm
		}

		if (usartHostToGsmState == 0x00)
		{
			if ((hostToGsmBuf_Wr != hostToGsmBuf_Rd) && (!flagHostToGsmTxEnable) && (!flagRecieveCRHostToGsm)) 			// ���� � ������ ���� ������, ������� ���������� ��������� �� ����� � GSM � �������� ��� �� ��������
			{
				while ( !(UCSR0A & (1<<UDRE0)) );											// ���� ����� ����������� ����������������
				flagHostToGsmTxEnable = 0x01;												// ������������� ����, ��� ��������� �������� �� ����� � GSM
				UDR0 = getCharFromHostToGsmBuf();
			}	
		}
			
		if (usartGsmToHostState == 0x00)													// E��� ����������� ����� �������� �� Gsm � �����
		{
		// ���� � ������ ���� ������, ������� ���������� ��������� �� GSM � �����
		// ���� �������� ��� �� ������ � � ������� ������ �������� ������� �� ������ ��� ���������� ��������� = 3,5 �������
			if ((gsmToHostBuf_Wr != gsmToHostBuf_Rd) && (flagGsmToHostTxEnable == 0) && (flagTimeInterval == 0x01))	
			{
				while( !(UCSR1A & (1<<UDRE1)) ); 											// ���� ���� ������������ ���������������� ����1
				//REDE = 1;																	// ����������� ���������������� �� ��������
				flagGsmToHostTxEnable = 0x01;												// ������������� ���� ������ �������� �� ����� � GSM
		    	UDR1 = getCharFromGsmToHostBuf();                                         	// �������� ��������
			}
		}
	}
}


//==========================================================================================
// ���������� �� ������������ �������2
//==========================================================================================
#pragma vector = TIMER2_COMP_vect
__interrupt void Timer2COMP(void)
{
	flagTimeInterval = 0x01;																// ������ ��������� �������� ������ 3.5 �������
}

//==========================================================================================
// ���������� �� ������������ �������0
//==========================================================================================
#pragma vector = TIMER0_COMP_vect
__interrupt void Timer0COMP(void)
{
	timer0cnt++;																			// ���� ������ ������� �������� ������ �� ������
	if(timer0cnt > timeoutRespFromGsm)
		flagTimeoutRespFromGsmOVR = 0x01;

	cntT0++;
	if (cntT0 > 2000)
		flagCntT0Ovf = 0x01;
}

//==========================================================================================
// ��������� GSM-������
//==========================================================================================
void powerUpModule(void)			
{
	//----������ ������---------------------------------------------------------------------
	__watchdog_reset();
	PWR_KEY = 0; 																			// �������� � 0
	__delay_cycles(7372800*0.8);															// ���� 1.2 ���
	while(STATUS != 0);																		// ������ ������ �� ��� ����� ����������� � ������ ����������� � 1
	PWR_KEY = 1; 																			// ������� � 1 
}

//==========================================================================================
// ������������ ������� 2
//==========================================================================================
void timer2Ini(unsigned char baud)
{
	TCNT2=0;																				// ����� ������� ��������
	switch (baud)
	{
		/*
		case BAUD2400:
		{
			OCR2 = 132; 																	// ���������� ��� �� ���������� OCR2, ������������ ����� 1.8 ���� 
			TCCR2 |= (1<<CS22)|(1<<CS21)|(1<<CS20)|(1<<WGM21);								// ������������ 1024, ����� ���
			break;
		}
		case BAUD4800:
		{
			OCR2 = 66; 																		// ���������� ��� �� ���������� OCR2, ������������ ����� 9.2 ���� 
			TCCR2 |= (1<<CS22)|(1<<CS21)|(1<<CS20)|(1<<WGM21);								// ������������ 1024, ����� ���
			break;
		}
		*/
		case BAUD9600:
		{
			OCR2 = 128; 																	// ���������� ��� �� ���������� OCR2, ������������ ����� 4.5 ���� 
			TCCR2 |= (1<<CS22)|(1<<CS21)|(1<<WGM21);										// ������������ 256, ����� ���
			break;
		}
		case BAUD19200:
		{
			OCR2 = 66; 																		// ���������� ��� �� ���������� OCR2, ������������ ����� 2.3 ���� 
			TCCR2 |= (1<<CS22)|(1<<CS21)|(1<<WGM21);										// ������������ 256, ����� ���
			break;
		}
		case BAUD38400:
		{
			OCR2 = 33; 																		// ���������� ��� �� ���������� OCR2, ������������ ����� 1.1 ���� 
			TCCR2 |= (1<<CS22)|(1<<CS21)|(1<<WGM21);										// ������������ 256, ����� ���
			break;
		}
		case BAUD57600:
		{
			OCR2 = 22; 																		// ���������� ��� �� ���������� OCR2, ������������ ����� 0.8 ���� 
			TCCR2 |= (1<<CS22)|(1<<CS21)|(1<<WGM21);										// ������������ 256, ����� ���
			break;
		}
		case BAUD115200:
		{
			OCR2 = 11; 																		// ���������� ��� �� ���������� OCR2, ������������ ����� 0.4 ���� 
			TCCR2 |= (1<<CS22)|(1<<CS21)|(1<<WGM21);										// ������������ 256, ����� ���
			break;
		}
		default:
		{
			OCR2 = 11; 																		// ���������� ��� �� ���������� OCR2, ������������ ����� 0.4 ���� 
			TCCR2 |= (1<<CS22)|(1<<CS21)|(1<<WGM21);										// ������������ 256, ����� ���
			break;
		}
	}
	TIMSK |= (1<<OCIE2);																	// ���������� ��� �� ���������� OCR2
}

//==========================================================================================
// ������������ ������� 2
//==========================================================================================
void timer0Ini(void)
{
	TCNT0=0;																				// ����� ������� ��������
	OCR0 = 128; 																			// ���������� ��� �� ���������� OCR2, ������������ ����� 4.5 ���� 
	TCCR0 |= (1<<CS02)|(1<<WGM01);															// ������������ 256, ����� ���
	TIMSK |= (1<<OCIE0);																	// ���������� ��� �� ���������� OCR2
}


//==========================================================================================
// ������ � ������
//==========================================================================================
unsigned char eepromWr (unsigned char AddrH, unsigned char Data)							// ������ � ������
{
	__watchdog_reset();
	__disable_interrupt();
	while(EECR & (1<<EEWE))																	/* Wait for completion of previous write */
		;
	EEAR = AddrH;																			/* Set up address and data registers */
	EEDR = Data;
	EECR |= (1<<EEMWE);																		/* Write logical one to EEMWE */
	EECR |= (1<<EEWE);																		/* Start eeprom write by setting EEWE */
	while(EECR & (1<<EEWE))																	/* Wait for completion of previous write */
		;
	EEAR = AddrH;																			/* Set up address register */
	EECR |= (1<<EERE);																		/* Start eeprom read by writing EERE */
	if (EEDR == Data)
	{
		__enable_interrupt();
		return 0; 
	}
	else
	{
		__enable_interrupt();
		return 1; 
	}
}


//==========================================================================================
// ������ �� ������
//==========================================================================================
unsigned char eepromRd (unsigned char AddrH)												// ������ �� ������		
{
	__watchdog_reset();
	__disable_interrupt();
	while(EECR & (1<<EEWE))																	/* Wait for completion of previous write */
		;
	EEAR = AddrH;																			/* Set up address register */
	EECR |= (1<<EERE);																		/* Start eeprom read by writing EERE */
	__enable_interrupt(); 																	/* Return data from data register */
	return EEDR;
}

//==========================================================================================
// ������������� ������� ������������ ������
//==========================================================================================
void timer1Ini(void)
{
	//TCNT1=0; 
	TIMSK|=(1<<TOIE1); 																		// ��������� ���������� �� ������� 1
}


//==========================================================================================
// ���������� �� ������������ �������1
//==========================================================================================
//=���������� ��� � 9 ������
#pragma vector = TIMER1_OVF_vect
__interrupt void Timer1OVF(void)
{
	timer1cnt++;
	if (timer1cnt > 400)																	// 400
	{
		timer1cnt = 0;
		timer1Ovf++;
	}

	if (timer1Ovf == timerTarget)															// ���� ��������� �������� ��������
	{
		powerUpModule();																	// ���������� ������������� �����
		__delay_cycles(16000000);
	}
}


//==========================================================================================
// ��������� ������� ������������ ������
//==========================================================================================
void setResetTimer(void)																	
{
	switch(eepromRd(4))																		
	// ������� ��������� ������� �� EEPROM, ���������� � ���������� � ���� ������� ������� ������������ � ��������� ������
	{
		case 0:
		{
			break;
		}
		case 1:
		{
			timerTarget = HOUR;
			TIMER1START;
			break;
		}
		case 2:
		{
			timerTarget = DAY;
			TIMER1START;
			break;
		}
		case 3:
		{
			timerTarget = WEEK;
			TIMER1START;
			break;
		}
		case 4:
		{
			timerTarget = MONTH;
			TIMER1START;
			break;
		}
		default:
		{
			break;
		}
	}
}