/*
* ������ GSM/GPRS ������ � RS-485 ����������� 
* � ���������� ������ ������� 220�->4�
*
* *
* MCU - ATmega162
* GSM/GPRS - SIM900
* RS-485 SP3485E
* Crystal Freq - 7.3728 MHz
* 
* ����������: ������ �.�.
* �����������: ��� "�������-��������"
* ���� ������: 18.05.2018
* ���� ����������: 
*/



//----����������������----------------------------------------------------------------------
#define SIZEOFRECIEVEBUF 	150
#define SMALLBUF   60

#define BAUD2400		191
#define BAUD4800		95
#define BAUD9600 		47
#define BAUD19200		23
#define BAUD38400		11
#define BAUD57600		7
#define BAUD115200		3

#define BAUD 			BAUD115200

//----������������--------------------------------------------------------------------------
void mcuToGsmUartInit (unsigned char baud);													// ������������� UART �� ������� GSM ������
void pcToMcuUartInit (unsigned char baud);													// ������������� UART �� ������� �����

void putCharFromHostToGsmBuf (unsigned char c);												// �������� ������ � ����� ��� �������� �� ����� � Gsm
void putCharFromGsmToHostBuf (unsigned char c);												// �������� ������ � ����� ��� �������� �� Gsm � �����

unsigned char getCharFromGsmToHostBuf (void);												// ����� ������ �� ������ ��� �������� �� ����� � GSM
unsigned char getCharFromHostToGsmBuf (void);												// ����� ������ �� ������ ��� �������� �� ����� � GSM

void printfToHost (char *string);															// ��������� ������ �������� �����
void printfToGsm (char *string);															// ��������� ������ �������� Gsm

unsigned char findCommandToGsm (char *string);												// ������������ ������ ������� � ������� �� ����� � Gsm, 
																							// ���������� ��������� ��������� 1 - �� ���������, 0 - ���������
unsigned char waitRespFromGsm (char *string, char *string1, unsigned char timeout);			// �������� ������ �� Gsm � �������� ����������
unsigned char parsRespFromGsm (char *string);												// ������ ������ �� GSM ������

//������  __flash
void printfToHostFl (char __flash *string);													// ��������� ������ �������� �����
void printfToGsmFl (char __flash *string);													// ��������� ������ �������� Gsm
unsigned char findCommandToGsmFl (char __flash *string);									// ������������ ������ ������� � ������� �� ����� � Gsm, 
																							// ���������� ��������� ��������� 1 - �� ���������, 0 - ���������
unsigned char waitRespFromGsmFl (char __flash *string, char __flash *string1, unsigned char timeout);	// �������� ������ �� Gsm � �������� ����������
unsigned char parsRespFromGsmFl (char __flash *string);										// ������ ������ �� GSM ������

void diagnosticMessage (char *string);														// ��������������� ���������

//----����������� �������-------------------------------------------------------------------
unsigned char strlen (char *string);														// ���������� ����� ������

//----����������----------------------------------------------------------------------------
unsigned char flagTimeInterval = 0x00;														// ���� - ������ ����� ������ 3.5 ������� (��� �������� 9600) ����� ������ �������� �������
unsigned char hostToGsmBuf[SIZEOFRECIEVEBUF];												// ����� �� ����� � GSM
unsigned char gsmToHostBuf[SIZEOFRECIEVEBUF];												// ����� GSM �����
unsigned char *hostToGsmBuf_Wr, *hostToGsmBuf_Rd;											// ��������� �� ������ ������ � ����� � ������ �� ������ � ��������� ������
unsigned char *gsmToHostBuf_Wr, *gsmToHostBuf_Rd;											// ��������� �� ������ ������ � ����� � ������ �� ������ � ��������� ������

unsigned char toHostBuf[SMALLBUF];															// ����� - �������� � �����
unsigned char *toHostBuf_Wr, *toHostBuf_Rd;													// ��������� �� ������� ���������� � ������� ��������� ������ � ������

unsigned char toGsmBuf[SMALLBUF];															// ����� - �������� � Gsm
unsigned char *toGsmBuf_Wr, *toGsmBuf_Rd;													// ��������� �� ������� ���������� � ������� ��������� ������ � ������

unsigned char answerFromGsmBuf[SIZEOFRECIEVEBUF];											// ����� - ������ ������ �� ������ Gsm
unsigned char *answerFromGsmBuf_Wr, *answerFromGsmBuf_Rd;									// ��������� �� ������� ���������� � ������� ��������� ������ � ������

unsigned char flagHostToGsmTxEnable = 0x00;                                         		// ���� ������� �������� �� USART 	 �� ����� � GSM ����������
unsigned char flagGsmToHostTxEnable = 0x00;                                         		// 									 �� GSM � ����� ����������

unsigned char usartHostToGsmState = 0x00;													// ��������� ������������������ 0x00 - �������� �������� �� ����� � GSM
																							//								0x01 - �������� �������� ������ � GSM
unsigned char usartGsmToHostState = 0x00;													// ��������� ������������������ 0x00 - �������� �������� �� Gsm � �����
																							//								0x01 - �������� �������� ������ � �����

unsigned char flagRecieveCRHostToGsm = 0x00;												// ���� - ��� ������ �� ����� CR-������
unsigned char *commandHostToGsmCR_Wr;														// ��������� - ����� ��������� �� ������ � ������� ����� ������ CR � �������� ������

unsigned char flagWaitRespFromGsm = 0x00;													// ���� ��������� ����� �� GSM ������
unsigned char timeoutRespFromGsm = 50;														// ������� ������ Gsm ������
unsigned char flagTimeoutRespFromGsmOVR = 0x00;												// ���� ������������ �������� ������ �� ������

__flash char AT_RESET[] ="AT+RESET\r";
__flash char AT_START[] ="AT+START\r";
__flash char AT_DIAGN[] ="AT+DIAGN\r";

__flash char AT_IPR0[] ="AT+IPR=0\r";
__flash char AT_IPR1200[] ="AT+IPR=1200\r";
__flash char AT_IPR2400[] ="AT+IPR=2400\r";
__flash char AT_IPR4800[] ="AT+IPR=4800\r";
__flash char AT_IPR9600[] ="AT+IPR=9600\r";
__flash char AT_IPR19200[] ="AT+IPR=19200\r";
__flash char AT_IPR38400[] ="AT+IPR=38400\r";
__flash char AT_IPR57600[] ="AT+IPR=57600\r";
__flash char AT_IPR115200[] ="AT+IPR=115200\r";

__flash char AT_ICF10[] ="AT+ICF=1,0\r";
__flash char AT_ICF11[] ="AT+ICF=1,1\r";
__flash char AT_ICF13[] ="AT+ICF=1,3\r";
__flash char AT_ICF20[] ="AT+ICF=2,0\r";
__flash char AT_ICF21[] ="AT+ICF=2,1\r";
__flash char AT_ICF23[] ="AT+ICF=2,3\r";
__flash char AT_ICF30[] ="AT+ICF=3,0\r";
__flash char AT_ICF31[] ="AT+ICF=3,1\r";
__flash char AT_ICF33[] ="AT+ICF=3,3\r";

__flash char AT_ICF_[] ="AT+ICF?\r";
__flash char AT_ICF20_[] ="\r\n+ICF: 2,0\r\n";
__flash char AT_ICF21_[] ="\r\n+ICF: 2,1\r\n";
__flash char AT_ICF33_[] ="\r\n+ICF: 3,3\r\n";
__flash char AT_ICF13_[] ="\r\n+ICF: 1,3\r\n";

__flash char AT_TIMER_0[] ="AT+TIMER=0\r";
__flash char AT_TIMER_1[] ="AT+TIMER=1\r";
__flash char AT_TIMER_2[] ="AT+TIMER=2\r";
__flash char AT_TIMER_3[] ="AT+TIMER=3\r";
__flash char AT_TIMER_4[] ="AT+TIMER=4\r";

__flash char AT_TIMER_[] ="AT+TIMER?\r";
__flash char AT_TIMER0[] ="\r\nAT+TIMER=0\r\n";
__flash char AT_TIMER1[] ="\r\nAT+TIMER=1\r\n";
__flash char AT_TIMER2[] ="\r\nAT+TIMER=2\r\n";
__flash char AT_TIMER3[] ="\r\nAT+TIMER=3\r\n";
__flash char AT_TIMER4[] ="\r\nAT+TIMER=4\r\n";

__flash char AT_IFC00[] ="AT+IFC=0,0\r";
__flash char AT_IFC01[] ="AT+IFC=0,1\r";
__flash char AT_IFC02[] ="AT+IFC=0,2\r";
__flash char AT_IFC10[] ="AT+IFC=1,0\r";
__flash char AT_IFC11[] ="AT+IFC=1,1\r";
__flash char AT_IFC12[] ="AT+IFC=1,2\r";
__flash char AT_IFC20[] ="AT+IFC=2,0\r";
__flash char AT_IFC21[] ="AT+IFC=2,1\r";
__flash char AT_IFC22[] ="AT+IFC=2,2\r";

__flash char AT_ICF40[] ="AT+ICF=4,0\r";
__flash char AT_ICF41[] ="AT+ICF=4,1\r";
__flash char AT_ICF43[] ="AT+ICF=4,3\r";
__flash char AT_ICF50[] ="AT+ICF=5,0\r";
__flash char AT_ICF51[] ="AT+ICF=5,1\r";
__flash char AT_ICF53[] ="AT+ICF=5,3\r";
__flash char AT_ICF60[] ="AT+ICF=6,0\r";
__flash char AT_ICF61[] ="AT+ICF=6,1\r";
__flash char AT_ICF63[] ="AT+ICF=6,3\r";

__flash char ATV0[] ="ATV0\r";
__flash char ATV1[] ="ATV1\r";



__flash char AT_W[] ="\r\nAT&W\r\n";

__flash char OK[] ="\r\nOK\r\n";
__flash char OK0[] ="0\r\n";
__flash char ERROR0[] ="4\r\n";
__flash char ERROR[] ="\r\nERROR\r\n";
