//------------------------------------------------------
//    Fuses.c    -   Read & Write Fuses bits
// Command Line to  Project option/Linker/Extra Options
// -Z(CODE)MyFuses=1F00-1FFF
//------------------------------------------------------
//#include <iom8.h>
#include <intrinsics.h>
#include "flash.h"
//------------------------------------------------------
// definitions for SPM control
#define	SPMCR_REG	SPMCR
//------------------------------------------------------
unsigned char s0;
void Fuses(void) @ "MyFuses"
{  unsigned char s1; s1=0xc0;
    _WAIT_FOR_SPM();
    s0=_GET_LOCK_BITS();	// Read lock bits.
//------------------------------------------------------
// if no lock bits then prog loock bits
    if(0x3c&s0)
    {	_WAIT_FOR_SPM();
	_SET_LOCK_BITS(s1);	// Write lock bits.
	_WAIT_FOR_SPM();
	s0=_GET_LOCK_BITS();	// Read lock bits.
    }
    _WAIT_FOR_SPM();
}
//------------------------------------------------------

//------------------------------------------------------
void SFuses(void)
{  if(0x03&s0)	// Test Lock on no protect
   {	
    //PORTD &=~ (1<<PD5); //включаем светодиод
    while(1)	    // Cicle indication "No Fuse"
      {
      }
   }
}
